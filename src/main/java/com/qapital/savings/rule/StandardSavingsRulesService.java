package com.qapital.savings.rule;

import com.qapital.bankdata.transaction.Transaction;
import com.qapital.bankdata.transaction.TransactionsService;
import com.qapital.savings.event.SavingsEvent;
import com.qapital.savings.event.SavingsEvent.EventName;
import com.qapital.savings.rule.SavingsRule.RuleType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class StandardSavingsRulesService implements SavingsRulesService {

    private final TransactionsService transactionsService;

    @Autowired
    public StandardSavingsRulesService(TransactionsService transactionsService) {
        this.transactionsService = transactionsService;
    }

    @Override
    public List<SavingsRule> activeRulesForUser(Long userId) {

        SavingsRule guiltyPleasureRule = SavingsRule.createGuiltyPleasureRule(1l, userId, "Starbucks", 3.00d);
        guiltyPleasureRule.addSavingsGoal(1l);
        guiltyPleasureRule.addSavingsGoal(2l);
        SavingsRule roundupRule = SavingsRule.createRoundupRule(2l, userId, 2.00d);
        roundupRule.addSavingsGoal(1l);

        return List.of(guiltyPleasureRule, roundupRule);
    }

    @Override
    public List<SavingsEvent> executeRule(SavingsRule savingsRule) {
        if (null == savingsRule)
            return List.of();
        var transactions = transactionsService.latestTransactionsForUser(savingsRule.getUserId());
        return transactions.stream()
            .filter(transaction -> transaction.getAmount() < 0)
            .flatMap(transaction -> appLySavingsRule(transaction, savingsRule).stream())
            .collect(Collectors.toList());

    }

    private List<SavingsEvent> appLySavingsRule(Transaction transaction, SavingsRule rule) {
        List<SavingsEvent> savingsEvents = new ArrayList<>();

        switch (rule.getRuleType()) {
            case guiltypleasure: 
                savingsEvents.addAll(guiltyPleasureSavings(transaction, rule));
                break;
            case roundup:
                savingsEvents.addAll(roundUpSavings(transaction, rule));
                break;
        }

        return savingsEvents;
    }

    private List<SavingsEvent> roundUpSavings(Transaction transaction, SavingsRule rule) {
        double roundedAmount = roundUp(transaction.getAmount(), rule.getAmount()) - Math.abs(transaction.getAmount());
        double savingsAmount = BigDecimal.valueOf(roundedAmount).setScale(2, RoundingMode.HALF_UP).doubleValue();

        return rule.getSavingsGoalIds().stream()
                .map(id -> createSavingsEvent(rule, transaction, savingsAmount / rule.getSavingsGoalIds().size(), id))
                .collect(Collectors.toList());
    }

    private List<SavingsEvent> guiltyPleasureSavings(Transaction transaction, SavingsRule rule) {
        if (transaction.getDescription().equals(rule.getPlaceDescription())) {
            double savingsAmount = rule.getAmount();
            return rule.getSavingsGoalIds().stream().map(
                    id -> createSavingsEvent(rule, transaction, savingsAmount / rule.getSavingsGoalIds().size(), id))
                    .collect(Collectors.toList());
        }
        return List.of();
    }

    private SavingsEvent createSavingsEvent(SavingsRule rule, Transaction transaction, Double amount, Long goalId) {

        return new SavingsEvent(rule.getUserId(), goalId, rule.getId(), EventName.rule_application,
                transaction.getDate(), amount, transaction.getId(), rule);

    }

    public static double roundUp(Double amount, Double roundUpValue) {

        if (roundUpValue == 0)
            return 0;
        var value = BigDecimal.valueOf(amount).abs();
        var upTo = BigDecimal.valueOf(roundUpValue);

        return value.divide(upTo, 0, RoundingMode.UP).multiply(upTo).doubleValue();
 
    }

}
