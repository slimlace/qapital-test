package com.qapital.savings.rule;

import com.qapital.bankdata.transaction.Transaction;
import com.qapital.bankdata.transaction.TransactionsService;
import com.qapital.savings.rule.SavingsRule.RuleType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import java.util.List;
import java.util.Map;
import java.time.LocalDate;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class SavingsRulesControllerTest {

  private static Long USERID = 1l;
  private MockMvc mockMvc;
  private final TransactionsService transactionsService = mock(TransactionsService.class);
  private final SavingsRulesService savingsRulesService = new StandardSavingsRulesService(transactionsService);
  private SavingsRulesController savingsRulesController;

  @BeforeEach
  void setUp() {
    savingsRulesController = new SavingsRulesController(savingsRulesService);
    mockMvc = standaloneSetup(savingsRulesController).build();
    when(transactionsService.latestTransactionsForUser(USERID)).thenReturn(
        List.of(new Transaction(Long.valueOf(1), USERID, Double.valueOf(-5.50d), "cafe", LocalDate.of(2021, 7, 1)),
            new Transaction(Long.valueOf(2), USERID, Double.valueOf(-2.20d), "cafe", LocalDate.of(2021, 7, 2)),
            new Transaction(Long.valueOf(3), USERID, Double.valueOf(-10.50d), "cinema", LocalDate.of(2021, 7, 3))));

  }

  @Test
  public void shouldReturnSavingsEventsAndOKForGuiltyPleasureRule() throws Exception {
    int EXPECTED_EVENTS_SIZE = 2;
    double EXPECTED_AMOUNT_PER_EVENT = 1.5;
   
    mockMvc
        .perform(post("/api/savings/rule/events").content(GUILTY_PLEASURE_RULE).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andExpect(jsonPath("$.size()").value(EXPECTED_EVENTS_SIZE))
        .andExpect(jsonPath("$[0].ruleType").value(RuleType.guiltypleasure.toString()))
        .andExpect(jsonPath("$[0].amount").value(EXPECTED_AMOUNT_PER_EVENT));

  }

  @Test
  public void shouldReturnSavingsEventsAndOKForRoundUpRule() throws Exception {
    int EXPECTED_EVENTS_SIZE = 3;
    List<Double> EXPECTED_SAVES = List.of(0.5, 1.8, 1.5);
    
   mockMvc
        .perform(post("/api/savings/rule/events").content(ROUND_UP_RULE).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andExpect(jsonPath("$.size()").value(EXPECTED_EVENTS_SIZE))
        .andExpect(jsonPath("$[*].amount").value(containsInAnyOrder(EXPECTED_SAVES.toArray())));
  
  }

  @Test
  public void failOnUnhandledRule() throws Exception {
 
   mockMvc
        .perform(post("/api/savings/rule/events").content(OTHER_RULE).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  
  }

  private static String GUILTY_PLEASURE_RULE = "{\n\"id\": 1,\n\"userId\": 1,\n\"placeDescription\": \"cinema\",\n\"amount\": 3,\n\"savingsGoalIds\": [\n  1,\n  2\n],\n\"ruleType\": \"guiltypleasure\",\n\"status\": \"active\",\n\"active\": true\n}";
  private static String ROUND_UP_RULE = "{\n\"id\": 2,\n\"userId\": 1,\n\"placeDescription\": null,\n\"amount\": 2,\n\"savingsGoalIds\": [\n  1\n],\n\"ruleType\": \"roundup\",\n\"status\": \"active\",\n\"active\": true\n}";
  private static String OTHER_RULE = "{\n\"id\": 2,\n\"userId\": 1,\n\"placeDescription\": null,\n\"amount\": 2,\n\"savingsGoalIds\": [\n  1\n],\n\"ruleType\": \"OTHER_RULE\",\n\"status\": \"active\",\n\"active\": true\n}";
}
